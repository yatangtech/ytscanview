//
//  YTAppDelegate.h
//  YTScanView
//
//  Created by YaTang_Dev_Hemin on 06/08/2017.
//  Copyright (c) 2017 YaTang_Dev_Hemin. All rights reserved.
//

@import UIKit;

@interface YTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
