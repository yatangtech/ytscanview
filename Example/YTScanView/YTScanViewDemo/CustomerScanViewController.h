//
//  CustomerScanViewController.h
//  YTScanDemo
//
//  Created by Min on 2017/6/7.
//  Copyright © 2017年 Min. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <YTScanView/YTScanView.h>
//#import <YTScanView/YTScan.h>
//#import <YTScanView/YT>
#import "YTPermissionsPlugin.h"
#import "LBXAlertAction.h"
//#import <YTScanView/YTS>
#import <YTScanView/YTScanView.h>
//#import <YTScanView/YTScan.h>

@interface CustomerScanViewController : UIViewController

/**
 * 扫码界面效果参数
 */
@property (nonatomic, strong) YTScanViewStyle *style;

@end
