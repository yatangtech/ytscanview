//
//  CustomerScanViewController.m
//  YTScanDemo
//
//  Created by Min on 2017/6/7.
//  Copyright © 2017年 Min. All rights reserved.
//

#import "CustomerScanViewController.h"

@interface CustomerScanViewController ()

/**
 * 扫码对象
 */
@property (nonatomic, strong) YTScan *scanObj;

/**
 * 扫码区域视图
 */
@property (nonatomic, strong) YTScanVisibleView *scanView;

@property (nonatomic, strong) UIView *videoView;

@end

@implementation CustomerScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = @"条形码识别";
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self drawScanView];
    //不延时，可能会导致界面黑屏并卡住一会儿
    [self performSelector:@selector(startScan) withObject:nil afterDelay:0.2];
}

// 绘制扫描区域
- (void)drawScanView {
    if (!_scanView) {
        CGRect rect = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 200);
//        rect.origin = CGPointMake(0, 0);
        self.scanView = [[YTScanVisibleView alloc] initWithFrame:rect style:_style];
        self.scanView.backgroundColor = [UIColor blackColor];
        [self.view addSubview:_scanView];
    }
    [_scanView startDeviceReadyingWithText:@"相机启动中"];
}

- (void)reStartDevice {
    [_scanObj startScan];
}

//启动设备
- (void)startScan {
    if (![YTPermissionsPlugin cameraPemission]) {
        [_scanView stopDeviceReadying];
        NSLog(@"请到设置隐私中开启本程序相机权限");
        return;
    }
    
    UIView *videoView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 200)];
    videoView.backgroundColor = [UIColor clearColor];
    [self.view insertSubview:videoView atIndex:0];
    __weak __typeof(self) weakSelf = self;
    if (!_scanObj) {
        CGRect cropRect = [YTScanVisibleView getScanRectWithPreView:self.view style:_style];
        //可识别的码的类型
        NSArray *strCodeArray = @[AVMetadataObjectTypeEAN13Code];
        self.scanObj = [[YTScan alloc] initWithPreView:videoView ObjectType:strCodeArray cropRect:cropRect success:^(NSArray<YTScanResult *> *array) {
            [weakSelf scanResultWithArray:array];
        }];
    }
    [_scanObj startScan];
    [_scanView stopDeviceReadying];
    _scanView.backgroundColor = [UIColor clearColor];
    [_scanView startScanAnimation];
}

- (void)stopScan {
    [_scanObj stopScan];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self stopScan];
    [_scanView stopScanAnimation];
}

- (void)showError:(NSString*)str {
    [LBXAlertAction showAlertWithTitle:@"提示" msg:str buttonsStatement:@[@"知道了"] chooseBlock:nil];
}

- (void)scanResultWithArray:(NSArray<YTScanResult *> *)array {
    if (array.count < 1) {
        [self popAlertMsgWithScanResult:nil];
        return;
    }
    for (YTScanResult *result in array) {
        NSLog(@"扫码结果：%@", result.strScanned);
    }
    YTScanResult *scanResult = array[0];
    NSString*strResult = scanResult.strScanned;
    
    //    self.scanImage = scanResult.imgScanned;
    
    if (!strResult) {
        
        [self popAlertMsgWithScanResult:nil];
        
        return;
    }
    
    [self showScanResult:scanResult];
    
}

- (void)popAlertMsgWithScanResult:(NSString*)strResult {
    if (!strResult) {
        
        strResult = @"识别失败";
    }
    
    __weak __typeof(self) weakSelf = self;
    [LBXAlertAction showAlertWithTitle:@"扫码内容" msg:strResult buttonsStatement:@[@"知道了"] chooseBlock:^(NSInteger buttonIdx) {
        
        [weakSelf reStartDevice];
    }];
}

- (void)showScanResult:(YTScanResult*)strResult {
    __weak __typeof(self) weakSelf = self;
    [LBXAlertAction showAlertWithTitle:@"扫码结果" msg:strResult.strScanned buttonsStatement:@[@"确定"] chooseBlock:^(NSInteger buttonIdx) {
        [weakSelf reStartDevice];
    }];
}


@end
