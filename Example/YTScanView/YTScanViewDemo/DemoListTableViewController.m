//
//  DemoListTableViewController.m
//  YTScanDemo
//
//  Created by Min on 2017/6/6.
//  Copyright © 2017年 Min. All rights reserved.
//

#import "DemoListTableViewController.h"
#import <objc/message.h>
#import "YTPermissionsPlugin.h"
#import <YTScanView/YTScanView.h>
#import <YTScanView/YTScan.h>
#import <YTScanView/YTScanViewStyle.h>
#import "FullScreenScanViewController.h"
#import "CustomerScanViewController.h"

@interface DemoListTableViewController ()<UINavigationControllerDelegate>
@property (nonatomic, strong) NSArray<NSArray *>* arrayItems;

@end

@implementation DemoListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
    [self arrayItems];
}

- (NSArray *)arrayItems {
    if (!_arrayItems) {
        NSArray *array = @[@[@"全屏扫描", @"allScreenScan"],@[@"自定义窗口扫描", @"customWindowScan"]];
        _arrayItems = array;
    }
    return _arrayItems;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrayItems.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.text = (NSString *)[_arrayItems[indexPath.row] firstObject];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *methodName = (NSString *)[_arrayItems[indexPath.row] lastObject];
    SEL normalSelector = NSSelectorFromString(methodName);
    if ([self respondsToSelector:normalSelector]) {
        ((void (*)(id, SEL))objc_msgSend)(self, normalSelector);
    }
}

#pragma mark ---自定义界面
- (void)openScanVCWithStyle:(YTScanViewStyle *)style {
    
}

- (void)allScreenScan {
    FullScreenScanViewController *vc = [[FullScreenScanViewController alloc] init];
    YTScanViewStyle *style = [[YTScanViewStyle alloc] init];
    style.centerUpOffset = 44;
    
    style.notRecoginitonArea = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    style.photoframeAngleStyle = YTScanViewPhotoframeAngleStyle_Outer;
    style.photoframeLineW = 6.0;
    style.photoframeAngleW = 24;
    style.photoframeAngleH = 24;
    //    style.isNeedShowRetangle = NO;
    style.animationStyle = YTScanViewAnimationStyle_NetGrid;
    UIImage *image = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_full_net"];
    style.animationImage = image;
    vc.style = style;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)customWindowScan {
    CustomerScanViewController *vc = [[CustomerScanViewController alloc] init];
    YTScanViewStyle *style = [[YTScanViewStyle alloc] init];
    style.centerUpOffset = 0;
    style.isNeedDrawPhotoframeAngle = NO;
    style.xScanRetangleOffset = 0;
    style.isNeedShowRetangle = NO;
    style.whRatio = CGRectGetWidth([UIScreen mainScreen].bounds)/200.0;
    style.animationStyle = YTScanViewAnimationStyle_Line;
    UIImage *image = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_light_green"];
    style.animationImage = image;
    vc.style = style;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
