//
//  FullScreenScanViewController.h
//  YTScanDemo
//
//  Created by Min on 2017/6/7.
//  Copyright © 2017年 Min. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTScanViewController.h"
#import "YTPermissionsPlugin.h"
#import "LBXAlertAction.h"

@interface FullScreenScanViewController : YTScanViewController

@end
