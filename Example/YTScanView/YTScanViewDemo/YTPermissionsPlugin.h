//
//  YTPermissionsPlugin.h
//  XCRIOS
//
//  Created by Jeavil_Tang on 2017/4/29.
//  Copyright © 2017年 YaTang Technology. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YTPermissionsPlugin : NSObject

+ (BOOL)cameraPemission;

+ (BOOL)photoPermission;

@end
