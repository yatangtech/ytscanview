//
//  YTScanViewController.h
//  YTScanDemo
//
//  Created by Min on 2017/6/7.
//  Copyright © 2017年 Min. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <YTScanView/YTScanView.h>
#import <YTScanView/YTScanView.h>
//#import <YTScanView/YTScan.h>
#import "YTPermissionsPlugin.h"

@interface YTScanViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

/**
 * 扫码界面效果参数
 */
@property (nonatomic, strong) YTScanViewStyle *style;
/**
 * 闪关灯开启状态
 */
@property(nonatomic, assign) BOOL isOpenFlash;

/**
 * 处理扫码后的结果
 */
- (void)scanResultWithArray:(NSArray <YTScanResult *>*)array;

//打开相册,暂未开放
- (void)openLocalPhoto;
//开关闪光灯
- (void)openOrCloseFlash;

- (void)reStartDevice;

@end
