//
//  YTScanViewController.m
//  YTScanDemo
//
//  Created by Min on 2017/6/7.
//  Copyright © 2017年 Min. All rights reserved.
//

#import "YTScanViewController.h"

@interface YTScanViewController ()

/**
 * 扫码对象
 */
@property (nonatomic, strong) YTScan *scanObj;

/**
 * 扫码区域视图
 */
@property (nonatomic, strong) YTScanVisibleView *scanView;

/**
 * 扫码区域CGRect
 */
@property (nonatomic, assign) CGRect scanRect;



@end

@implementation YTScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.view.backgroundColor = [UIColor blackColor];
    
    self.title = @"二维码/条形码扫描";
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self drawScanView];
    //不延时，可能会导致界面黑屏并卡住一会儿
    [self performSelector:@selector(startScan) withObject:nil afterDelay:0.2];
}

// 绘制扫描区域
- (void)drawScanView {
    if (!_scanView) {
        CGRect rect = self.view.frame;
        rect.origin = CGPointMake(0, 0);
        self.scanView = [[YTScanVisibleView alloc] initWithFrame:rect style:_style];
        [self.view addSubview:_scanView];
    }
    [_scanView startDeviceReadyingWithText:@"相机启动中"];
}

- (void)reStartDevice {
    [_scanObj startScan];
}

//启动设备
- (void)startScan {
    if (![YTPermissionsPlugin cameraPemission]) {
        [_scanView stopDeviceReadying];
        NSLog(@"请到设置隐私中开启本程序相机权限");
        return;
    }
    
    UIView *videoView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
    videoView.backgroundColor = [UIColor clearColor];
    [self.view insertSubview:videoView atIndex:0];
    __weak __typeof(self) weakSelf = self;
    if (!_scanObj) {
        CGRect cropRect = [YTScanVisibleView getScanRectWithPreView:self.view style:_style];
        //可识别的码的类型
        NSArray *strCodeArray = @[AVMetadataObjectTypeQRCode, AVMetadataObjectTypeEAN13Code];
        self.scanObj = [[YTScan alloc] initWithPreView:videoView ObjectType:strCodeArray cropRect:cropRect success:^(NSArray<YTScanResult *> *array) {
            [weakSelf scanResultWithArray:array];
        }];
    }
    [_scanObj startScan];
    [_scanView stopDeviceReadying];
    [_scanView startScanAnimation];
}

- (void)stopScan {
    [_scanObj stopScan];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self stopScan];
    [_scanView stopScanAnimation];
}


#pragma mark -实现类继承该方法，作出对应处理

- (void)scanResultWithArray:(NSArray <YTScanResult *>*)array {
    if (!array || array.count < 1) {
        NSLog(@"识别失败");
        return ;
    }
    YTScanResult *result = array[0];
    NSString *strResult = result.strScanned;
    NSLog(@"扫码结果：%@", strResult);
}

- (void)openOrCloseFlash {
    [_scanObj changeTorch];
    self.isOpenFlash = !self.isOpenFlash;
}

#pragma mark --打开相册并识别图片
/*!
 *  打开本地照片，选择图片识别
 */
- (void)openLocalPhoto {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}

#pragma mark -UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    __block UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    if (!image) {
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    __weak __typeof(self) weakSelf = self;
    if ([[[UIDevice currentDevice]systemVersion]floatValue] >= 8.0) {
        [YTScan recognizeImage:image success:^(NSArray<YTScanResult *> *array) {
            [weakSelf scanResultWithArray:array];
        }];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    NSLog(@"cancel");
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}



@end
