//
//  main.m
//  YTScanView
//
//  Created by YaTang_Dev_Hemin on 06/08/2017.
//  Copyright (c) 2017 YaTang_Dev_Hemin. All rights reserved.
//

@import UIKit;
#import "YTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YTAppDelegate class]));
    }
}
