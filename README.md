# YTScanView

[![CI Status](http://img.shields.io/travis/YaTang_Dev_Hemin/YTScanView.svg?style=flat)](https://travis-ci.org/YaTang_Dev_Hemin/YTScanView)
[![Version](https://img.shields.io/cocoapods/v/YTScanView.svg?style=flat)](http://cocoapods.org/pods/YTScanView)
[![License](https://img.shields.io/cocoapods/l/YTScanView.svg?style=flat)](http://cocoapods.org/pods/YTScanView)
[![Platform](https://img.shields.io/cocoapods/p/YTScanView.svg?style=flat)](http://cocoapods.org/pods/YTScanView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YTScanView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "YTScanView"
```

## Author

YaTang_Dev_Hemin, 286888980@qq.com

## License

YTScanView is available under the MIT license. See the LICENSE file for more info.
