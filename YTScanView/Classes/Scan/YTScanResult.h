//
//  YTScanResult.h
//  YTScanDemo
//
//  Created by Min on 2017/6/6.
//  Copyright © 2017年 Min. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIkit.h>

@interface YTScanResult : NSObject

- (instancetype)initWithScanString:(NSString *)str imgScan:(UIImage *)img barCodeType:(NSString *)type;

/**
 @brief 条码字符串
 */
@property (nonatomic, copy) NSString* strScanned;
/**
 @brief  扫码图像
 */
@property (nonatomic, strong) UIImage* imgScanned;
/**
 @brief 扫码类型
 */
@property (nonatomic, copy) NSString* strBarCodeType;

@end
