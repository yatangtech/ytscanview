//
//  YTScanResult.m
//  YTScanDemo
//
//  Created by Min on 2017/6/6.
//  Copyright © 2017年 Min. All rights reserved.
//

#import "YTScanResult.h"

@implementation YTScanResult

- (instancetype)initWithScanString:(NSString *)str imgScan:(UIImage *)img barCodeType:(NSString *)type {
    if (self = [super init]) {
        self.strScanned = str;
        self.imgScanned = img;
        self.strBarCodeType = type;
    }
    return self;
}

@end
