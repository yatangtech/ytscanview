//
//  YTScanLineAnimation.h
//  YTScanDemo
//
//  Created by Min on 2017/6/6.
//  Copyright © 2017年 Min. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YTScanLineAnimation : UIImageView

/**
 * 线条扫描动画
 * @param animationRect 显示在parentView中得区域
 * @param parentView    动画显示在UIView
 * @param image         扫码线的图像
 # @param height        扫码线的高度
 */
- (void)startAnimatingWithRect:(CGRect)animationRect InView:(UIView *)parentView Image:(UIImage *)image ScanLineHeight:(CGFloat)height;

/**
 * 停止动画
 */
- (void)stopAnimating;

@end
