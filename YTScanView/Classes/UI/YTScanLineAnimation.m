//
//  YTScanLineAnimation.m
//  YTScanDemo
//
//  Created by Min on 2017/6/6.
//  Copyright © 2017年 Min. All rights reserved.
//

#import "YTScanLineAnimation.h"

@interface YTScanLineAnimation()
{
    int num;
    BOOL down;
    NSTimer * timer;
    
    BOOL isAnimationing;
}

@property (nonatomic, assign) CGRect animationRect;
@property (nonatomic, assign) CGFloat scanLinehHeight;

@end

@implementation YTScanLineAnimation

- (void)startAnimatingWithRect:(CGRect)animationRect InView:(UIView *)parentView Image:(UIImage *)image ScanLineHeight:(CGFloat)height {
    if (isAnimationing) {
        return;
    }
    isAnimationing = YES;
    self.animationRect = animationRect;
    self.scanLinehHeight = height;
    CGFloat leftx = animationRect.origin.x + 5;
    CGFloat width = animationRect.size.width - 10;
    self.frame = CGRectMake(leftx, 0, width, height);
    self.image = image;
    [parentView addSubview:self];
    [self startLineAnimation];
}

- (void)startLineAnimation {
    self.hidden = NO;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
    animation.fromValue = @(self.animationRect.origin.y);
    animation.toValue = @(self.animationRect.size.height+self.animationRect.origin.y-self.scanLinehHeight);
    animation.duration = self.animationRect.size.height/200*2.5;
    animation.repeatCount = OPEN_MAX;
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [self.layer addAnimation:animation forKey:@"scanLineAnimationName"];
}

- (void)stopAnimating {
    if (isAnimationing) {
        isAnimationing = NO;
        self.hidden = YES;
        [self.layer removeAnimationForKey:@"scanLineAnimationName"];
    }
}

- (void)dealloc {
    [self stopAnimating];
}

@end
