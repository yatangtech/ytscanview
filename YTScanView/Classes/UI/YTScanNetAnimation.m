//
//  YTScanNetAnimation.m
//  YTScanDemo
//
//  Created by Min on 2017/6/6.
//  Copyright © 2017年 Min. All rights reserved.
//

#import "YTScanNetAnimation.h"

@interface YTScanNetAnimation()
{
    BOOL isAnimationing;
}

@property (nonatomic, assign) CGRect animationRect;
@property (nonatomic, strong) UIImageView *scanImageView;

@end

@implementation YTScanNetAnimation

- (instancetype)init {
    self = [super init];
    if (self) {
        self.clipsToBounds = YES;
        [self addSubview:self.scanImageView];
    }
    return self;
}

- (UIImageView *)scanImageView {
    if (!_scanImageView) {
        _scanImageView = [[UIImageView alloc] init];
    }
    return _scanImageView;
}

- (void)startAnimatingWithRect:(CGRect)animationRect InView:(UIView *)parentView Image:(UIImage *)image {
    [self.scanImageView setImage:image];
    self.animationRect = animationRect;
    self.frame = animationRect;
    [parentView addSubview: self];
    CGFloat scanNetImageViewW = animationRect.size.width;
    CGFloat scanNetImageH = animationRect.size.height;
    _scanImageView.frame = CGRectMake(0, -scanNetImageH, scanNetImageViewW, scanNetImageH);
    self.hidden = NO;
    isAnimationing = YES;
    [self startNetAnimation];
}

- (void)startNetAnimation {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
    animation.fromValue = @(0);
    animation.toValue = @(self.animationRect.size.height+2);
    animation.duration = self.animationRect.size.height/200*2;
    animation.repeatCount = OPEN_MAX;
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [self.scanImageView.layer addAnimation:animation forKey:@"scanNetAnimationName"];
}

- (void)stopAnimating {
    self.hidden = YES;
    isAnimationing = NO;
    [self.scanImageView.layer removeAnimationForKey:@"scanNetAnimationName"];
}

- (void)dealloc {
    [self stopAnimating];
}

@end
