//
//  YTScanViewStyle.m
//  YTScanDemo
//
//  Created by Min on 2017/6/6.
//  Copyright © 2017年 Min. All rights reserved.
//

#import "YTScanViewStyle.h"

@implementation YTScanViewStyle

- (id)init {
    if (self = [super init]) {
        _isNeedShowRetangle = YES;
        _isNeedShowReadyingText = YES;
        _isNeedDrawPhotoframeAngle = YES;
        _whRatio = 1.0;
        _colorRetangleLine = [UIColor whiteColor];
        _centerUpOffset = 44;
        _xScanRetangleOffset = 60;
        _animationStyle = YTScanViewAnimationStyle_Line;
        _photoframeAngleStyle = YTScanViewPhotoframeAngleStyle_Outer;
        _colorAngle = [UIColor colorWithRed:0 green:167.0/255 blue:231.0/255 alpha:1.0];
        _notRecoginitonArea = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        _photoframeAngleW = 24;
        _photoframeAngleH = 24;
        _photoframeLineW = 7;
        _scanLineHeight = 2.5;
    }
    return self;
}

@end
