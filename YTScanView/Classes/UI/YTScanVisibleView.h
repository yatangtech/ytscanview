//
//  YTScanVisibleView.h
//  Pods
//
//  Created by Min on 2017/6/20.
//
//

#import <UIKit/UIKit.h>
#import "YTScanLineAnimation.h"
#import "YTScanNetAnimation.h"
#import "YTScanViewStyle.h"

@interface YTScanVisibleView : UIView

- (id)initWithFrame:(CGRect)frame style:(YTScanViewStyle *)style;

/**
 *  设备启动中文字提示
 */
- (void)startDeviceReadyingWithText:(NSString *)text;

/**
 *  设备启动完成
 */
- (void)stopDeviceReadying;

/**
 *  开始扫描动画
 */
- (void)startScanAnimation;

/**
 *  结束扫描动画
 */
- (void)stopScanAnimation;

/**
 * 根据矩形区域，获取扫码识别兴趣区域
 * @param view  视频流显示UIView
 * @param style 效果界面参数
 * @return 识别区域
 */
+ (CGRect)getScanRectWithPreView:(UIView*)view style:(YTScanViewStyle *)style;

@end
